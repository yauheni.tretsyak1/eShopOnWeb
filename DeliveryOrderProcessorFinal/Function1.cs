﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace DeliveryOrderProcessorFinal
{
    public static class Function1
    {
        [FunctionName("OrderItemsReserverFinal")]
        public static async Task Run1(
                        [ServiceBusTrigger("%ServiceBusQueueName%")]
                    string myQueueItem,
                        int deliveryCount,
                        DateTime enqueuedTimeUtc,
                        string messageId,
                        [Blob("final-exam/{sys.utcnow}.json", FileAccess.Write)] Stream outputBlob,
                        ILogger log)
        {
            log.LogInformation($"myQueueItem {myQueueItem},deliveryCount {deliveryCount}");

            if (deliveryCount > 3 || Environment.GetEnvironmentVariable("ThrowException") == "true")
            {
                await SentEmailAsync(myQueueItem, Environment.GetEnvironmentVariable("LogicAppUrl"));
            }

            using (StreamWriter writer = new StreamWriter(outputBlob))
            {
                await writer.WriteAsync(myQueueItem);
            }
        }

        private static async Task SentEmailAsync(string value, string url)
        {
            using var client = new HttpClient();
            var data = new StringContent(value, Encoding.UTF8, "application/json");

            await client.PostAsync(url, data);
        }

        [FunctionName("OrderItemsReserverCosmosDb")]
        public static async Task<IActionResult> Run2(
                [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
                [CosmosDB(
                databaseName: "ToDoList",
                containerName: "Orders",
                Connection = "CosmosDBConnection")]IAsyncCollector<dynamic> documentsOut,
                ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            OrderMapped newOrder = JsonConvert.DeserializeObject<OrderMapped>(requestBody);

            await documentsOut.AddAsync(newOrder);

            return new OkObjectResult("Ok Cosmos");
        }

        private class OrderMapped
        {
            public string id { get; set; }
            public string buyerId { get; set; }
            public int orderItemsCount { get; set; }
            public string shipTo { get; set; }
        }
    }
}
