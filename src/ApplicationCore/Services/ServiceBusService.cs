﻿using System.Net.Http;
using System.Text.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.eShopWeb.ApplicationCore.Entities.OrderAggregate;

namespace Microsoft.eShopWeb.ApplicationCore.Services;
public class ServiceBusService
{
    private IConfiguration _configuratio;
    public ServiceBusService(IConfiguration configuratio)
    {
        _configuratio = configuratio;
    }

    public async Task SentAsync(Order order)
    {
        await using var client = new ServiceBusClient(_configuratio["ServiceBusUrl"]);
        ServiceBusSender sender = client.CreateSender(_configuratio["ServiceBusQueueName"]);

        var json = JsonSerializer.Serialize<Order>(order);
        var message = new ServiceBusMessage(json);

        await sender.SendMessageAsync(message);
    }
}
