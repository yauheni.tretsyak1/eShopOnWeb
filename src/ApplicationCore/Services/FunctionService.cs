﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.eShopWeb.ApplicationCore.Entities.OrderAggregate;
using Microsoft.Extensions.Configuration;

namespace Microsoft.eShopWeb.ApplicationCore.Services;
public class FunctionService
{
    private IConfiguration _configuratio;

    public FunctionService(Microsoft.Extensions.Configuration.IConfiguration configuratio)
    {
        _configuratio = configuratio;
    }
    public async Task SentAsync(Order order)
    {
        using var client = new HttpClient();
        var json = JsonSerializer.Serialize<OrderMapped>(
                new OrderMapped { 
                    id= order.Id.ToString(), 
                    buyerId = order.BuyerId, 
                    orderItemsCount = order.OrderItems.Count,
                    shipTo = $"{order.ShipToAddress.Country}, {order.ShipToAddress.State}, {order.ShipToAddress.City}, {order.ShipToAddress.ZipCode}" 
                }
            );
        var data = new StringContent(json, Encoding.UTF8, "application/json");

        await client.PostAsync(_configuratio["AzureFunctionUrl"], data);
    }

    private class OrderMapped
    {
        public string id { get; set; }
        public string buyerId { get; set; }
        public int orderItemsCount { get; set; }
        public string shipTo { get; set; }
    }
}
